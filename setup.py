from distutils.core import setup
setup(
  name = 'lisa_sdk',
  packages = ['lisa_sdk'],
  version = '0.2.0',
  description = 'Software Development Kit of the LISA project (Lightweight Improvable Software Assistant)',
  author = 'Xavier Saliniere',
  author_email = 'xavier.saliniere@openmailbox.org',
  url = 'https://bitbucket.org/lisadevteam/lisa-sdk',
  download_url = 'https://bitbucket.org/lisadevteam/lisa-sdk/get/master.zip',
  keywords = ['sdk', 'lisa'],
  classifiers = [],
)
