# Lisa SDK
Software Development Kit of the LISA project (Lightweight Improvable Software Assistant).
## Description
This package contains interface that must be implemented to develop Lisa plugins.
## Contributors
* Xavier Saliniere
* Kéviinraj Balaratnam
* Brayan Gonzalez
* Emanuel Legendre
## Licence
GNU General Public License